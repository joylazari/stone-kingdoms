---@enum keyEvents
local EVENTS = {
    IncreaseGameSpeed  = "IncreaseGameSpeed",
    NormalizeGameSpeed = "NormalizeGameSpeed",
    DecreaseGameSpeed  = "DecreaseGameSpeed",
    Screenshot         = "Screenshot",
    Escape             = "Escape",
    ToggleDebugView    = "ToggleDebugView",
    CenterViewToKeep   = "CenterViewToKeep",
    CamUp              = "CamUp",
    CamLeft            = "CamLeft",
    CamDown            = "CamDown",
    CamRight           = "CamRight",
    ActionBar1         = "ActionBar1",
    ActionBar2         = "ActionBar2",
    ActionBar3         = "ActionBar3",
    ActionBar4         = "ActionBar4",
    ActionBar5         = "ActionBar5",
    ActionBar6         = "ActionBar6",
    ActionBar7         = "ActionBar7",
    ActionBar8         = "ActionBar8",
    ActionBar9         = "ActionBar9",
    ActionBar10         = "ActionBar10",
    ActionBar11         = "ActionBar11",
    ActionBar12         = "ActionBar12",
    RightClick          =  2,
    LeftClick           =  1
}

return EVENTS
