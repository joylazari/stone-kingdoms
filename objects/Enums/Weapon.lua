---@enum weapon
local WEAPON = {
    bow = "bow",
    crossbow = "crossbow",
    spear = "spear",
    pike = "pike",
    mace = "mace",
    sword = "sword",
    leatherArmor = "leather armor",
    shield = "shield",
}

return WEAPON
