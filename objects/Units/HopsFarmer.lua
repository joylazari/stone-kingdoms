local _, _ = ...
local Worker = require("objects.Units.Worker")
local Object = require("objects.Object")
local indexQuads = _G.indexQuads
local anim = _G.anim

local harvestFx = {_G.fx["harvest_01"], _G.fx["harvest_02"], _G.fx["harvest_03"], _G.fx["harvest_04"],
    _G.fx["harvest_05"], _G.fx["harvest_06"]}

local hoeFx = {_G.fx["hoe_01"], _G.fx["hoe_02"], _G.fx["hoe_03"], _G.fx["hoe_04"], _G.fx["hoe_05"], _G.fx["hoe_06"],
    _G.fx["hoe_07"]}

local fr = {
    walking_apples_east = indexQuads("body_farmer_walk_apples_e", 16),
    walking_apples_north = indexQuads("body_farmer_walk_apples_n", 16),
    walking_apples_west = indexQuads("body_farmer_walk_apples_w", 16),
    walking_apples_south = indexQuads("body_farmer_walk_apples_s", 16),
    walking_apples_northeast = indexQuads("body_farmer_walk_apples_ne", 16),
    walking_apples_northwest = indexQuads("body_farmer_walk_apples_nw", 16),
    walking_apples_southeast = indexQuads("body_farmer_walk_apples_se", 16),
    walking_apples_southwest = indexQuads("body_farmer_walk_apples_sw", 16),
    walking_hops_east = indexQuads("body_farmer_walk_hops_e", 16),
    walking_hops_north = indexQuads("body_farmer_walk_hops_n", 16),
    walking_hops_west = indexQuads("body_farmer_walk_hops_w", 16),
    walking_hops_south = indexQuads("body_farmer_walk_hops_s", 16),
    walking_hops_northeast = indexQuads("body_farmer_walk_hops_ne", 16),
    walking_hops_northwest = indexQuads("body_farmer_walk_hops_nw", 16),
    walking_hops_southeast = indexQuads("body_farmer_walk_hops_se", 16),
    walking_hops_southwest = indexQuads("body_farmer_walk_hops_sw", 16),
    walking_east = indexQuads("body_farmer_walk_e", 16),
    walking_north = indexQuads("body_farmer_walk_n", 16),
    walking_northeast = indexQuads("body_farmer_walk_ne", 16),
    walking_northwest = indexQuads("body_farmer_walk_nw", 16),
    walking_south = indexQuads("body_farmer_walk_s", 16),
    walking_southeast = indexQuads("body_farmer_walk_se", 16),
    walking_southwest = indexQuads("body_farmer_walk_sw", 16),
    walking_west = indexQuads("body_farmer_walk_w", 16),
    -- hoe
    gather_walk_hoe_north = indexQuads("body_farmer_hoe_n", 8),
    gather_walk_hoe_west = indexQuads("body_farmer_hoe_w", 8),
    gather_walk_hoe_south = indexQuads("body_farmer_hoe_s", 8),
    gather_walk_hoe_east = indexQuads("body_farmer_hoe_e", 8),
    gather_walk_hoe_northeast = indexQuads("body_farmer_hoe_ne", 8),
    gather_walk_hoe_northwest = indexQuads("body_farmer_hoe_nw", 8),
    gather_walk_hoe_southeast = indexQuads("body_farmer_hoe_se", 8),
    gather_walk_hoe_southwest = indexQuads("body_farmer_hoe_sw", 8),
    gather_hoe_north = indexQuads("body_farmer_hoe_n", 12, 9),
    gather_hoe_west = indexQuads("body_farmer_hoe_w", 12, 9),
    gather_hoe_south = indexQuads("body_farmer_hoe_s", 12, 9),
    gather_hoe_east = indexQuads("body_farmer_hoe_e", 12, 9),
    gather_hoe_northeast = indexQuads("body_farmer_hoe_ne", 12, 9),
    gather_hoe_northwest = indexQuads("body_farmer_hoe_nw", 12, 9),
    gather_hoe_southeast = indexQuads("body_farmer_hoe_se", 12, 9),
    gather_hoe_southwest = indexQuads("body_farmer_hoe_sw", 12, 9),
    gather_hoe_part2_north = indexQuads("body_farmer_hoe_n", 16, 13),
    gather_hoe_part2_west = indexQuads("body_farmer_hoe_w", 16, 13),
    gather_hoe_part2_south = indexQuads("body_farmer_hoe_s", 16, 13),
    gather_hoe_part2_east = indexQuads("body_farmer_hoe_e", 16, 13),
    gather_hoe_part2_northeast = indexQuads("body_farmer_hoe_ne", 16, 13),
    gather_hoe_part2_northwest = indexQuads("body_farmer_hoe_nw", 16, 13),
    gather_hoe_part2_southeast = indexQuads("body_farmer_hoe_se", 16, 13),
    gather_hoe_part2_southwest = indexQuads("body_farmer_hoe_sw", 16, 13),
    -- earthing
    gather_earthing_north = indexQuads("body_farmer_earth_n", 8),
    gather_earthing_west = indexQuads("body_farmer_earth_w", 8),
    gather_earthing_south = indexQuads("body_farmer_earth_s", 8),
    gather_earthing_northeast = indexQuads("body_farmer_earth_ne", 8),
    gather_earthing_northwest = indexQuads("body_farmer_earth_nw", 8),
    gather_earthing_southeast = indexQuads("body_farmer_earth_se", 8),
    gather_earthing_southwest = indexQuads("body_farmer_earth_sw", 8),
    -- planting
    gather_planting_north = indexQuads("body_farmer_seed_n", 16),
    gather_planting_west = indexQuads("body_farmer_seed_w", 16),
    gather_planting_south = indexQuads("body_farmer_seed_s", 16),
    gather_planting_east = indexQuads("body_farmer_seed_e", 16),
    gather_planting_northeast = indexQuads("body_farmer_seed_ne", 16),
    gather_planting_northwest = indexQuads("body_farmer_seed_nw", 16),
    gather_planting_southeast = indexQuads("body_farmer_seed_se", 16),
    gather_planting_southwest = indexQuads("body_farmer_seed_sw", 16),
    -- walk hops
    gather_walk_hops_north = indexQuads("body_farmer_walk_hops_n", 16),
    gather_walk_hops_west = indexQuads("body_farmer_walk_hops_w", 16),
    gather_walk_hops_south = indexQuads("body_farmer_walk_hops_s", 16),
    gather_walk_hops_northeast = indexQuads("body_farmer_walk_hops_ne", 16),
    gather_walk_hops_northwest = indexQuads("body_farmer_walk_hops_nw", 16),
    gather_walk_hops_southeast = indexQuads("body_farmer_walk_hops_se", 16),
    gather_walk_hops_southwest = indexQuads("body_farmer_walk_hops_sw", 16),
    -- idle

    idle = indexQuads("body_farmer_idle", 16),
    idle_loop = indexQuads("body_farmer_idle", 16, 12, true)
}

local AN = {
    WALKING_APPLES_EAST = "Walking_Apples_East",
    WALKING_APPLES_NORTH = "Walking_Apples_North",
    WALKING_APPLES_WEST = "Walking_Apples_West",
    WALKING_APPLES_SOUTH = "Walking_Apples_South",
    WALKING_APPLES_NORTHEAST = "Walking_Apples_Northeast",
    WALKING_APPLES_NORTHWEST = "Walking_Apples_Northwest",
    WALKING_APPLES_SOUTHEAST = "Walking_Apples_Southeast",
    WALKING_APPLES_SOUTHWEST = "Walking_Apples_Southwest",
    WALKING_WHEAT_EAST = "Walking_Hops_East",
    WALKING_WHEAT_NORTH = "Walking_Hops_North",
    WALKING_WHEAT_WEST = "Walking_Hops_West",
    WALKING_WHEAT_SOUTH = "Walking_Hops_South",
    WALKING_WHEAT_NORTHEAST = "Walking_Hops_Northeast",
    WALKING_WHEAT_NORTHWEST = "Walking_Hops_Northwest",
    WALKING_WHEAT_SOUTHEAST = "Walking_Hops_Southeast",
    WALKING_WHEAT_SOUTHWEST = "Walking_Hops_Southwest",
    WALKING_EAST = "Walking_East",
    WALKING_NORTH = "Walking_North",
    WALKING_NORTHEAST = "Walking_Northeast",
    WALKING_NORTHWEST = "Walking_Northwest",
    WALKING_SOUTH = "Walking_South",
    WALKING_SOUTHEAST = "Walking_Southeast",
    WALKING_SOUTHWEST = "Walking_Southwest",
    WALKING_WEST = "Walking_West",
    -- hoe
    GATHER_WALK_HOE_NORTH = "Gather_Walk_Hoe_North",
    GATHER_WALK_HOE_WEST = "Gather_Walk_Hoe_West",
    GATHER_WALK_HOE_SOUTH = "Gather_Walk_Hoe_South",
    GATHER_WALK_HOE_EAST = "Gather_Walk_Hoe_East",
    GATHER_WALK_HOE_NORTHEAST = "Gather_Walk_Hoe_Northeast",
    GATHER_WALK_HOE_NORTHWEST = "Gather_Walk_Hoe_Northwest",
    GATHER_WALK_HOE_SOUTHEAST = "Gather_Walk_Hoe_Southeast",
    GATHER_WALK_HOE_SOUTHWEST = "Gather_Walk_Hoe_Southwest",
    GATHER_HOE_NORTH = "Gather_Hoe_North",
    GATHER_HOE_WEST = "Gather_Hoe_West",
    GATHER_HOE_SOUTH = "Gather_Hoe_South",
    GATHER_HOE_EAST = "Gather_Hoe_East",
    GATHER_HOE_NORTHEAST = "Gather_Hoe_Northeast",
    GATHER_HOE_NORTHWEST = "Gather_Hoe_Northwest",
    GATHER_HOE_SOUTHEAST = "Gather_Hoe_Southeast",
    GATHER_HOE_SOUTHWEST = "Gather_Hoe_Southwest",
    GATHER_HOE_PART2_NORTH = "Gather_Hoe_Part2_North",
    GATHER_HOE_PART2_WEST = "Gather_Hoe_Part2_West",
    GATHER_HOE_PART2_SOUTH = "Gather_Hoe_Part2_South",
    GATHER_HOE_PART2_EAST = "Gather_Hoe_Part2_East",
    GATHER_HOE_PART2_NORTHEAST = "Gather_Hoe_Part2_Northeast",
    GATHER_HOE_PART2_NORTHWEST = "Gather_Hoe_Part2_Northwest",
    GATHER_HOE_PART2_SOUTHEAST = "Gather_Hoe_Part2_Southeast",
    GATHER_HOE_PART2_SOUTHWEST = "Gather_Hoe_Part2_Southwest",
    -- earthing
    GATHER_EARTHING_NORTH = "Gather_Earthing_North",
    GATHER_EARTHING_WEST = "Gather_Earthing_West",
    GATHER_EARTHING_SOUTH = "Gather_Earthing_South",
    GATHER_EARTHING_NORTHEAST = "Gather_Earthing_Northeast",
    GATHER_EARTHING_NORTHWEST = "Gather_Earthing_Northwest",
    GATHER_EARTHING_SOUTHEAST = "Gather_Earthing_Southeast",
    GATHER_EARTHING_SOUTHWEST = "Gather_Earthing_Southwest",
    -- planting
    GATHER_PLANTING_NORTH = "Gather_Planting_North",
    GATHER_PLANTING_WEST = "Gather_Planting_West",
    GATHER_PLANTING_SOUTH = "Gather_Planting_South",
    GATHER_PLANTING_EAST = "Gather_Planting_East",
    GATHER_PLANTING_NORTHEAST = "Gather_Planting_Northeast",
    GATHER_PLANTING_NORTHWEST = "Gather_Planting_Northwest",
    GATHER_PLANTING_SOUTHEAST = "Gather_Planting_Southeast",
    GATHER_PLANTING_SOUTHWEST = "Gather_Planting_Southwest",
    -- walk hops
    GATHER_WALK_WHEAT_NORTH = "Gather_Walk_Hops_North",
    GATHER_WALK_WHEAT_WEST = "Gather_Walk_Hops_West",
    GATHER_WALK_WHEAT_SOUTH = "Gather_Walk_Hops_South",
    GATHER_WALK_WHEAT_NORTHEAST = "Gather_Walk_Hops_Northeast",
    GATHER_WALK_WHEAT_NORTHWEST = "Gather_Walk_Hops_Northwest",
    GATHER_WALK_WHEAT_SOUTHEAST = "Gather_Walk_Hops_Southeast",
    GATHER_WALK_WHEAT_SOUTHWEST = "Gather_Walk_Hops_Southwest",
    -- idle
    IDLE = "Idle",
    IDLE_LOOP = "Idle_Loop"
}

local an = {
    [AN.WALKING_APPLES_EAST] = fr.walking_apples_east,
    [AN.WALKING_APPLES_NORTH] = fr.walking_apples_north,
    [AN.WALKING_APPLES_WEST] = fr.walking_apples_west,
    [AN.WALKING_APPLES_SOUTH] = fr.walking_apples_south,
    [AN.WALKING_APPLES_NORTHEAST] = fr.walking_apples_northeast,
    [AN.WALKING_APPLES_NORTHWEST] = fr.walking_apples_northwest,
    [AN.WALKING_APPLES_SOUTHEAST] = fr.walking_apples_southeast,
    [AN.WALKING_APPLES_SOUTHWEST] = fr.walking_apples_southwest,
    [AN.WALKING_WHEAT_EAST] = fr.walking_hops_east,
    [AN.WALKING_WHEAT_NORTH] = fr.walking_hops_north,
    [AN.WALKING_WHEAT_WEST] = fr.walking_hops_west,
    [AN.WALKING_WHEAT_SOUTH] = fr.walking_hops_south,
    [AN.WALKING_WHEAT_NORTHEAST] = fr.walking_hops_northeast,
    [AN.WALKING_WHEAT_NORTHWEST] = fr.walking_hops_northwest,
    [AN.WALKING_WHEAT_SOUTHEAST] = fr.walking_hops_southeast,
    [AN.WALKING_WHEAT_SOUTHWEST] = fr.walking_hops_southwest,
    [AN.WALKING_EAST] = fr.walking_east,
    [AN.WALKING_NORTH] = fr.walking_north,
    [AN.WALKING_NORTHEAST] = fr.walking_northeast,
    [AN.WALKING_NORTHWEST] = fr.walking_northwest,
    [AN.WALKING_SOUTH] = fr.walking_south,
    [AN.WALKING_SOUTHEAST] = fr.walking_southeast,
    [AN.WALKING_SOUTHWEST] = fr.walking_southwest,
    [AN.WALKING_WEST] = fr.walking_west,
    -- hoe
    [AN.GATHER_WALK_HOE_NORTH] = fr.gather_walk_hoe_north,
    [AN.GATHER_WALK_HOE_WEST] = fr.gather_walk_hoe_west,
    [AN.GATHER_WALK_HOE_SOUTH] = fr.gather_walk_hoe_south,
    [AN.GATHER_WALK_HOE_EAST] = fr.gather_walk_hoe_east,
    [AN.GATHER_WALK_HOE_NORTHEAST] = fr.gather_walk_hoe_northeast,
    [AN.GATHER_WALK_HOE_NORTHWEST] = fr.gather_walk_hoe_northwest,
    [AN.GATHER_WALK_HOE_SOUTHEAST] = fr.gather_walk_hoe_southeast,
    [AN.GATHER_WALK_HOE_SOUTHWEST] = fr.gather_walk_hoe_southwest,
    [AN.GATHER_HOE_NORTH] = fr.gather_hoe_north,
    [AN.GATHER_HOE_WEST] = fr.gather_hoe_west,
    [AN.GATHER_HOE_SOUTH] = fr.gather_hoe_south,
    [AN.GATHER_HOE_EAST] = fr.gather_hoe_east,
    [AN.GATHER_HOE_NORTHEAST] = fr.gather_hoe_northeast,
    [AN.GATHER_HOE_NORTHWEST] = fr.gather_hoe_northwest,
    [AN.GATHER_HOE_SOUTHEAST] = fr.gather_hoe_southeast,
    [AN.GATHER_HOE_SOUTHWEST] = fr.gather_hoe_southwest,
    [AN.GATHER_HOE_PART2_NORTH] = fr.gather_hoe_part2_north,
    [AN.GATHER_HOE_PART2_WEST] = fr.gather_hoe_part2_west,
    [AN.GATHER_HOE_PART2_SOUTH] = fr.gather_hoe_part2_south,
    [AN.GATHER_HOE_PART2_EAST] = fr.gather_hoe_part2_east,
    [AN.GATHER_HOE_PART2_NORTHEAST] = fr.gather_hoe_part2_northeast,
    [AN.GATHER_HOE_PART2_NORTHWEST] = fr.gather_hoe_part2_northwest,
    [AN.GATHER_HOE_PART2_SOUTHEAST] = fr.gather_hoe_part2_southeast,
    [AN.GATHER_HOE_PART2_SOUTHWEST] = fr.gather_hoe_part2_southwest,
    -- earthing
    [AN.GATHER_EARTHING_NORTH] = fr.gather_earthing_north,
    [AN.GATHER_EARTHING_WEST] = fr.gather_earthing_west,
    [AN.GATHER_EARTHING_SOUTH] = fr.gather_earthing_south,
    [AN.GATHER_EARTHING_NORTHEAST] = fr.gather_earthing_northeast,
    [AN.GATHER_EARTHING_NORTHWEST] = fr.gather_earthing_northwest,
    [AN.GATHER_EARTHING_SOUTHEAST] = fr.gather_earthing_southeast,
    [AN.GATHER_EARTHING_SOUTHWEST] = fr.gather_earthing_southwest,
    -- planting
    [AN.GATHER_PLANTING_NORTH] = fr.gather_planting_north,
    [AN.GATHER_PLANTING_WEST] = fr.gather_planting_west,
    [AN.GATHER_PLANTING_SOUTH] = fr.gather_planting_south,
    [AN.GATHER_PLANTING_EAST] = fr.gather_planting_east,
    [AN.GATHER_PLANTING_NORTHEAST] = fr.gather_planting_northeast,
    [AN.GATHER_PLANTING_NORTHWEST] = fr.gather_planting_northwest,
    [AN.GATHER_PLANTING_SOUTHEAST] = fr.gather_planting_southeast,
    [AN.GATHER_PLANTING_SOUTHWEST] = fr.gather_planting_southwest,
    -- walk hops
    [AN.GATHER_WALK_WHEAT_NORTH] = fr.gather_walk_hops_north,
    [AN.GATHER_WALK_WHEAT_WEST] = fr.gather_walk_hops_west,
    [AN.GATHER_WALK_WHEAT_SOUTH] = fr.gather_walk_hops_south,
    [AN.GATHER_WALK_WHEAT_NORTHEAST] = fr.gather_walk_hops_northeast,
    [AN.GATHER_WALK_WHEAT_NORTHWEST] = fr.gather_walk_hops_northwest,
    [AN.GATHER_WALK_WHEAT_SOUTHEAST] = fr.gather_walk_hops_southeast,
    [AN.GATHER_WALK_WHEAT_SOUTHWEST] = fr.gather_walk_hops_southwest,
    -- idle
    [AN.IDLE] = fr.idle,
    [AN.IDLE_LOOP] = fr.idle_loop
}

local HopsFarmer = _G.class("HopsFarmer", Worker)
function HopsFarmer:initialize(gx, gy, type)
    Worker.initialize(self, gx, gy, type)
    self.workplace = nil
    self.state = "Find a job"
    self.marked = 0
    self.count = 1
    self.farmlandTiles = {}
    self.offsetY = -10
    self.offsetX = -5
    self.timr = 0
    self.animated = true
    self.animation = anim.newAnimation(an[AN.WALKING_WEST], 10, nil, AN.WALKING_WEST)
    self.hops = 0
end

function HopsFarmer:dirSubUpdate()
    if self.state == "Working" then
        return
    end
    if self.moveDir == "west" then
        if self.state == "Going to stockpile" or (self.state == "Going to pick up hops" and self.hops > 0) then
            self.animation = anim.newAnimation(an[AN.WALKING_WHEAT_WEST], 0.05, nil, AN.WALKING_WHEAT_WEST)
        elseif self.state == "Going to seed the land" then
            self.animation = anim.newAnimation(an[AN.WALKING_APPLES_WEST], 0.05, nil, AN.WALKING_APPLES_WEST)
        else
            self.animation = anim.newAnimation(an[AN.WALKING_WEST], 0.05, nil, AN.WALKING_WEST)
        end
    elseif self.moveDir == "southwest" then
        if self.state == "Going to stockpile" or (self.state == "Going to pick up hops" and self.hops > 0) then
            self.animation = anim.newAnimation(an[AN.WALKING_WHEAT_SOUTHWEST], 0.05, nil, AN.WALKING_WHEAT_SOUTHWEST)
        elseif self.state == "Going to seed the land" then
            self.animation = anim.newAnimation(an[AN.WALKING_APPLES_SOUTHWEST], 0.05, nil, AN.WALKING_APPLES_SOUTHWEST)
        else
            self.animation = anim.newAnimation(an[AN.WALKING_SOUTHWEST], 0.05, nil, AN.WALKING_SOUTHWEST)
        end
    elseif self.moveDir == "northwest" then
        if self.state == "Going to stockpile" or (self.state == "Going to pick up hops" and self.hops > 0) then
            self.animation = anim.newAnimation(an[AN.WALKING_WHEAT_NORTHWEST], 0.05, nil, AN.WALKING_WHEAT_NORTHWEST)
        elseif self.state == "Going to seed the land" then
            self.animation = anim.newAnimation(an[AN.WALKING_APPLES_NORTHWEST], 0.05, nil, AN.WALKING_APPLES_NORTHWEST)
        else
            self.animation = anim.newAnimation(an[AN.WALKING_NORTHWEST], 0.05, nil, AN.WALKING_NORTHWEST)
        end
    elseif self.moveDir == "north" then
        if self.state == "Going to stockpile" or (self.state == "Going to pick up hops" and self.hops > 0) then
            self.animation = anim.newAnimation(an[AN.WALKING_WHEAT_NORTH], 0.05, nil, AN.WALKING_WHEAT_NORTH)
        elseif self.state == "Going to seed the land" then
            self.animation = anim.newAnimation(an[AN.WALKING_APPLES_NORTH], 0.05, nil, AN.WALKING_APPLES_NORTH)
        else
            self.animation = anim.newAnimation(an[AN.WALKING_NORTH], 0.05, nil, AN.WALKING_NORTH)
        end
    elseif self.moveDir == "south" then
        if self.state == "Going to stockpile" or (self.state == "Going to pick up hops" and self.hops > 0) then
            self.animation = anim.newAnimation(an[AN.WALKING_WHEAT_SOUTH], 0.05, nil, AN.WALKING_WHEAT_SOUTH)
        elseif self.state == "Going to seed the land" then
            self.animation = anim.newAnimation(an[AN.WALKING_APPLES_SOUTH], 0.05, nil, AN.WALKING_APPLES_SOUTH)
        else
            self.animation = anim.newAnimation(an[AN.WALKING_SOUTH], 0.05, nil, AN.WALKING_SOUTH)
        end
    elseif self.moveDir == "east" then
        if self.state == "Going to stockpile" or (self.state == "Going to pick up hops" and self.hops > 0) then
            self.animation = anim.newAnimation(an[AN.WALKING_WHEAT_EAST], 0.05, nil, AN.WALKING_WHEAT_EAST)
        elseif self.state == "Going to seed the land" then
            self.animation = anim.newAnimation(an[AN.WALKING_APPLES_EAST], 0.05, nil, AN.WALKING_APPLES_EAST)
        else
            self.animation = anim.newAnimation(an[AN.WALKING_EAST], 0.05, nil, AN.WALKING_EAST)
        end
    elseif self.moveDir == "southeast" then
        if self.state == "Going to stockpile" or (self.state == "Going to pick up hops" and self.hops > 0) then
            self.animation = anim.newAnimation(an[AN.WALKING_WHEAT_SOUTHEAST], 0.05, nil, AN.WALKING_WHEAT_SOUTHEAST)
        elseif self.state == "Going to seed the land" then
            self.animation = anim.newAnimation(an[AN.WALKING_APPLES_SOUTHEAST], 0.05, nil, AN.WALKING_APPLES_SOUTHEAST)
        else
            self.animation = anim.newAnimation(an[AN.WALKING_SOUTHEAST], 0.05, nil, AN.WALKING_SOUTHEAST)
        end
    elseif self.moveDir == "northeast" then
        if self.state == "Going to stockpile" or (self.state == "Going to pick up hops" and self.hops > 0) then
            self.animation = anim.newAnimation(an[AN.WALKING_WHEAT_NORTHEAST], 0.05, nil, AN.WALKING_WHEAT_NORTHEAST)
        elseif self.state == "Going to seed the land" then
            self.animation = anim.newAnimation(an[AN.WALKING_APPLES_NORTHEAST], 0.05, nil, AN.WALKING_APPLES_NORTHEAST)
        else
            self.animation = anim.newAnimation(an[AN.WALKING_NORTHEAST], 0.05, nil, AN.WALKING_NORTHEAST)
        end
    end
end

function HopsFarmer:jobUpdate()
    _G.removeObjectAt(self.lrcx, self.lrcy, self.lrx, self.lry, self)
end

function HopsFarmer:anchorWorkPosition()
    if not self.waypointX or not self.waypointY then
        return
    end
    self.fx = self.waypointX * 1000
    self.fy = self.waypointY * 1000
    self.gy = math.round(self.fx * 0.001)
    self.gy = math.round(self.fy * 0.001)
end

function HopsFarmer:load(data)
    Object.deserialize(self, data)
    Worker.load(self, data)
    local farmlandTiles = {}
    data.farmlandTiles = data.farmlandTiles or {}
    for _, ftile in ipairs(data.farmlandTiles) do
        if ftile == false then
            farmlandTiles[#farmlandTiles + 1] = ftile
        else
            farmlandTiles[#farmlandTiles + 1] = _G.state:dereferenceObject(ftile)
        end
    end
    self.farmlandTiles = farmlandTiles
    if data.resourceTile then
        self.resourceTile = _G.state:dereferenceObject(data.resourceTile)
    end
    local anData = data.animation
    if anData then
        local animId = anData.animationIdentifier
        local seedLandAnimations = {
            [AN.GATHER_PLANTING_SOUTH] = true,
            [AN.GATHER_PLANTING_NORTH] = true,
            [AN.GATHER_PLANTING_EAST] = true,
            [AN.GATHER_PLANTING_NORTHEAST] = true,
            [AN.GATHER_PLANTING_SOUTHEAST] = true
        }
        local callback
        if seedLandAnimations[animId] then
            -- seed land sequence
            callback = function()
                self:seedLandCallback()
            end
        end
        local hoeLandAnimations_1 = {
            [AN.GATHER_WALK_HOE_SOUTH] = true,
            [AN.GATHER_WALK_HOE_NORTH] = true,
            [AN.GATHER_WALK_HOE_EAST] = true,
            [AN.GATHER_WALK_HOE_NORTHEAST] = true,
            [AN.GATHER_WALK_HOE_SOUTHEAST] = true
        }
        if hoeLandAnimations_1[animId] then
            -- hoe land sequence 1
            callback = function()
                self:hoeLandCallback(1)
            end
        end
        local hoeLandAnimations_2 = {
            [AN.GATHER_HOE_SOUTH] = true,
            [AN.GATHER_HOE_NORTH] = true,
            [AN.GATHER_HOE_EAST] = true,
            [AN.GATHER_HOE_NORTHEAST] = true,
            [AN.GATHER_HOE_SOUTHEAST] = true
        }
        if hoeLandAnimations_2[animId] then
            -- hoe land sequence 2
            callback = function()
                self:hoeLandCallback(2)
            end
        end
        local hoeLandAnimations_3 = {
            [AN.GATHER_HOE_PART2_SOUTH] = true,
            [AN.GATHER_HOE_PART2_NORTH] = true,
            [AN.GATHER_HOE_PART2_EAST] = true,
            [AN.GATHER_HOE_PART2_NORTHEAST] = true,
            [AN.GATHER_HOE_PART2_SOUTHEAST] = true
        }
        if hoeLandAnimations_3[animId] then
            -- hoe land sequence 3
            callback = function()
                self:hoeLandCallback(3)
            end
        end
        self.animation = anim.newAnimation(an[anData.animationIdentifier], 1, callback, anData.animationIdentifier)
        self.animation:deserialize(anData)
    end
    if self.path == 2 then
        self.path = 0
        self:clearPath()
        self:requestPath(self.endx, self.endy)
    end
end

function HopsFarmer:serialize()
    local data = {}
    local unitData = Worker.serialize(self)
    for k, v in pairs(unitData) do
        if type(v) ~= "function" and type(v) ~= "userdata" then
            data[k] = v
        end
    end
    if self.animation then
        data.animation = self.animation:serialize()
    end
    data.state = self.state
    data.count = self.count
    data.offsetX = self.offsetX
    data.offsetY = self.offsetY
    data.hops = self.hops
    local farmlandTilesRaw = {}
    for _, tile in ipairs(self.farmlandTiles) do
        if tile then
            farmlandTilesRaw[#farmlandTilesRaw + 1] = _G.state:serializeObject(tile)
        else
            farmlandTilesRaw[#farmlandTilesRaw + 1] = tile
        end
    end
    if #farmlandTilesRaw > 0 then
        data.farmlandTiles = farmlandTilesRaw
    end
    if self.resourceTile then
        data.resourceTile = _G.state:serializeObject(self.resourceTile)
    end
    return data
end

function HopsFarmer:hoeLandCallback(state)
    local _, anim2, anim3 = self:hoeLandGetAnim()
    local function state_3Callback()
        self.straightWalkSpeed = 2400
        self.diagonalWalkSpeed = self.straightWalkSpeed * 1.414
        self:updatePosition()
        self.animation:pause()
        self.workplace:work(self)
        self:clearPath()
    end

    if state == 1 then
        self:anchorWorkPosition()
        self:updatePosition()
        self.moveDir = "none"
        self.animation = anim.newAnimation(an[anim2], 0.1, function()
            _G.playSfx(self, hoeFx)
            self.workplace:updateTiles(self.farmlandTiles)
            self.animation = anim.newAnimation(an[anim3], 0.1, function(self)
                state_3Callback()
                self:pauseAtEnd()
            end, anim3)
        end, anim2)
    elseif state == 2 then
        self.workplace:updateTiles(self.farmlandTiles)
        _G.playSfx(self, hoeFx)
        self.animation = anim.newAnimation(an[anim3], 0.1, function(self)
            state_3Callback()
            self:pauseAtEnd()
        end, anim3)
    elseif state == 3 then
        state_3Callback()
    else
        error("Received unknown state for hoe_land: " .. tostring(state))
    end
end

function HopsFarmer:hoeLandGetAnim()
    local anim1, anim2, anim3
    if self.state == "Working" then
        if self.moveDir == nil or self.moveDir == "none" then
            if string.find(self.animation.animationIdentifier, "South") then
                anim1 = AN.GATHER_WALK_HOE_SOUTH
                anim2 = AN.GATHER_HOE_SOUTH
                anim3 = AN.GATHER_HOE_PART2_SOUTH
            elseif string.find(self.animation.animationIdentifier, "North") then
                anim1 = AN.GATHER_WALK_HOE_NORTH
                anim2 = AN.GATHER_HOE_NORTH
                anim3 = AN.GATHER_HOE_PART2_NORTH
            elseif string.find(self.animation.animationIdentifier, "East") then
                anim1 = AN.GATHER_WALK_HOE_EAST
                anim2 = AN.GATHER_HOE_EAST
                anim3 = AN.GATHER_HOE_PART2_EAST
            elseif string.find(self.animation.animationIdentifier, "Northeast") then
                anim1 = AN.GATHER_WALK_HOE_NORTHEAST
                anim2 = AN.GATHER_WALK_HOE_NORTHEAST
                anim3 = AN.GATHER_HOE_PART2_NORTHEAST
            elseif string.find(self.animation.animationIdentifier, "Southeast") then
                anim1 = AN.GATHER_WALK_HOE_SOUTHEAST
                anim2 = AN.GATHER_HOE_SOUTHEAST
                anim3 = AN.GATHER_HOE_PART2_SOUTHEAST
            end
        elseif self.moveDir == "south" then
            anim1 = AN.GATHER_WALK_HOE_SOUTH
            anim2 = AN.GATHER_HOE_SOUTH
            anim3 = AN.GATHER_HOE_PART2_SOUTH
        elseif self.moveDir == "north" then
            anim1 = AN.GATHER_WALK_HOE_NORTH
            anim2 = AN.GATHER_HOE_NORTH
            anim3 = AN.GATHER_HOE_PART2_NORTH
        elseif self.moveDir == "east" then
            anim1 = AN.GATHER_WALK_HOE_EAST
            anim2 = AN.GATHER_HOE_EAST
            anim3 = AN.GATHER_HOE_PART2_EAST
        elseif self.moveDir == "northeast" then
            anim1 = AN.GATHER_WALK_HOE_NORTHEAST
            anim2 = AN.GATHER_HOE_NORTHEAST
            anim3 = AN.GATHER_HOE_PART2_NORTHEAST
            if self.currentTile and self:isPositionAt(self.currentTile.gx - 1, self.currentTile.gy + 2) then
                anim2 = AN.GATHER_HOE_NORTH
                anim3 = AN.GATHER_HOE_PART2_NORTH
            end
        elseif self.moveDir == "southeast" then
            anim1 = AN.GATHER_WALK_HOE_SOUTHEAST
            anim2 = AN.GATHER_HOE_SOUTHEAST
            anim3 = AN.GATHER_HOE_PART2_SOUTHEAST
        end
    else
        anim1, anim2, anim3, _, _, _ = self:hoeLandPreprocess()
    end
    if not anim1 then
        print("num 1", self.moveDir)
        error("bro")
    end
    if not anim2 then
        print("num 2", self.moveDir)
        error("bro")
    end
    if not anim3 then
        print("num 3", self.moveDir)
        error("bro")
    end
    return anim1, anim2, anim3
end

function HopsFarmer:hoeLandPreprocess()
    local anim1, anim2, anim3, skipWalking
    local futureWaypointX, futureWaypointY = self.gx, self.gy
    if self.state == "Going to hoe the land from south" or self.state == "Going to hoe the land from north" then
        skipWalking = true
    end
    if self.state == "Hoe walking to southern tile" or self.state == "Going to hoe the land from north" then
        self.moveDir = "south"
        anim1 = AN.GATHER_WALK_HOE_SOUTH
        anim2 = AN.GATHER_HOE_SOUTH
        anim3 = AN.GATHER_HOE_PART2_SOUTH
        futureWaypointY = self.gy + 1
    elseif self.state == "Hoe walking to northern tile" or self.state == "Going to hoe the land from south" then
        self.moveDir = "north"
        anim1 = AN.GATHER_WALK_HOE_NORTH
        anim2 = AN.GATHER_HOE_NORTH
        anim3 = AN.GATHER_HOE_PART2_NORTH
        futureWaypointY = self.gy - 1
    elseif self.state == "Hoe walking to eastern tile" or self.state == "Going to hoe the land from east" then
        self.moveDir = "east"
        anim1 = AN.GATHER_WALK_HOE_EAST
        anim2 = AN.GATHER_HOE_EAST
        anim3 = AN.GATHER_HOE_PART2_EAST
        futureWaypointX = self.gx + 1
    elseif self.state == "Hoe walking to northeastern tile" then
        self.moveDir = "northeast"
        anim1 = AN.GATHER_WALK_HOE_NORTHEAST
        anim2 = AN.GATHER_HOE_NORTHEAST
        anim3 = AN.GATHER_HOE_PART2_NORTHEAST
        futureWaypointX = self.gx + 1
        futureWaypointY = self.gy - 1
        if self.currentTile and self:isPositionAt(self.currentTile.gx - 1, self.currentTile.gy + 1) then
            skipWalking = true
            futureWaypointX = self.gx
            futureWaypointY = self.gy
        elseif self.currentTile and self:isPositionAt(self.currentTile.gx - 2, self.currentTile.gy + 1) then
            skipWalking = true
            futureWaypointX = self.gx
            futureWaypointY = self.gy
        elseif self.currentTile and self:isPositionAt(self.currentTile.gx - 1, self.currentTile.gy + 2) then
            anim2 = AN.GATHER_HOE_NORTH
            anim3 = AN.GATHER_HOE_PART2_NORTH
        end
    elseif self.state == "Hoe walking to southeastern tile" then
        self.moveDir = "southeast"
        anim1 = AN.GATHER_WALK_HOE_SOUTHEAST
        anim2 = AN.GATHER_HOE_SOUTHEAST
        anim3 = AN.GATHER_HOE_PART2_SOUTHEAST
        futureWaypointX = self.gx + 1
        futureWaypointY = self.gy + 1
    end
    return anim1, anim2, anim3, skipWalking, futureWaypointX, futureWaypointY
end

function HopsFarmer:hoeLand()
    local anim1, anim2, _, skipWalking, futureWaypointX, futureWaypointY = self:hoeLandPreprocess()
    self.state = "Working"
    self.hasMoveDir = true
    self.straightWalkSpeed = 0
    self.animation:resume()
    self.waypointX, self.waypointY = futureWaypointX, futureWaypointY
    if skipWalking then
        self.animation = anim.newAnimation(an[anim2], 0.1, function()
            self:hoeLandCallback(2)
        end, anim2)
    else
        self.straightWalkSpeed = 1276.7
        self.diagonalWalkSpeed = self.straightWalkSpeed * 1.414 * 1
        self:updatePosition()
        self.animation = anim.newAnimation(an[anim1], 0.1, function()
            self:hoeLandCallback(1)
        end, anim1)
    end
end

function HopsFarmer:seedLandCallback()
    self:anchorWorkPosition()
    self:updatePosition()
    self.moveDir = "none"
    self.straightWalkSpeed = 2400
    self.diagonalWalkSpeed = self.straightWalkSpeed * 1.414
    self.animation:pause()
    self.workplace:updateTiles(self.farmlandTiles)
    self.workplace:work(self)
    self:clearPath()
end

function HopsFarmer:seedLand()
    local anim1, skipWalking
    local futureWaypointX, futureWaypointY = self.gx, self.gy
    if self.state == "Going to seed the land from south" or self.state == "Going to seed the land from north" then
        skipWalking = true
    end
    if self.state == "Seed walking to southern tile" or self.state == "Going to seed the land from north" then
        self.moveDir = "south"
        anim1 = AN.GATHER_PLANTING_SOUTH
        futureWaypointY = self.gy + 1
    elseif self.state == "Seed walking to northern tile" or self.state == "Going to seed the land from south" then
        self.moveDir = "north"
        anim1 = AN.GATHER_PLANTING_NORTH
        futureWaypointY = self.gy - 1
    elseif self.state == "Seed walking to eastern tile" or self.state == "Going to seed the land from east" then
        self.moveDir = "east"
        anim1 = AN.GATHER_PLANTING_EAST
        futureWaypointX = self.gx + 1
    elseif self.state == "Seed walking to northeastern tile" then
        self.moveDir = "northeast"
        anim1 = AN.GATHER_PLANTING_NORTHEAST
        futureWaypointX = self.gx + 1
        futureWaypointY = self.gy - 1
        if self.currentTile and self:isPositionAt(self.currentTile.gx - 1, self.currentTile.gy + 1) then
            skipWalking = true
            futureWaypointX = self.gx
            futureWaypointY = self.gy
        elseif self.currentTile and self:isPositionAt(self.currentTile.gx - 2, self.currentTile.gy + 1) then
            skipWalking = true
            futureWaypointX = self.gx
            futureWaypointY = self.gy
        end
    elseif self.state == "Seed walking to southeastern tile" then
        self.moveDir = "southeast"
        anim1 = AN.GATHER_PLANTING_SOUTHEAST
        futureWaypointX = self.gx + 1
        futureWaypointY = self.gy + 1
    end
    self.state = "Working"
    self.hasMoveDir = true
    self.animation:resume()
    self.waypointX, self.waypointY = futureWaypointX, futureWaypointY
    if skipWalking then
        self.workplace:updateTiles(self.farmlandTiles)
        self.workplace:work(self)
        self:clearPath()
    else
        self.straightWalkSpeed = 638.4
        self.diagonalWalkSpeed = self.straightWalkSpeed * 1.414
        self.animation = anim.newAnimation(an[anim1], 0.1, function()
            self:seedLandCallback()
        end, anim1)
    end
end

function HopsFarmer:update()
    if self.pathState == "Waiting for path" and self.state ~= "Working" and self.state ~= "Resting" then
        self:pathfind()
    elseif self.state == "Working" and self.moveDir ~= "none" then
        self:move(true)
    elseif self.state == "Go to rest" then
        self.animation = anim.newAnimation(an[AN.IDLE], 0.1, "pauseAtEnd", AN.IDLE)
        self.state = "Resting"
    elseif self.state == "Resting" then
        self.workplace:work(self)
    elseif self.state ~= "No path to farm" then
        if self.state == "Find a job" then
            _G.JobController:findJob(self, "HopsFarmer")
        elseif self.state == "Go to stockpile" then
            if _G.stockpile then
                self.state = "Going to stockpile"
                local closestNode
                local distance = math.huge
                for _, v in ipairs(_G.stockpile.nodeList) do
                    local tmp = _G.manhattanDistance(v.gx, v.gy, self.gx, self.gy)
                    if tmp < distance then
                        distance = tmp
                        closestNode = v
                    end
                end
                if not closestNode then
                    print("Closest stockpile node not found")
                else
                    self:requestPath(closestNode.gx, closestNode.gy)
                end
                self.moveDir = "none"
            end
        elseif self.state == "Go to workplace" then
            self:requestPath(self.workplace.gx, self.workplace.gy + 4)
            self.state = "Going to workplace"
            self.moveDir = "none"
        elseif self.moveDir == "none" and _G.string.startsWith(self.state, "Going") then
            self:updateDirection()
        end
        if _G.string.startsWith(self.state, "Going") then
            self:move()
        end
        if self.state == "Hoe walking to southern tile" or self.state == "Hoe walking to northern tile" or self.state ==
            "Hoe walking to eastern tile" or self.state == "Hoe walking to southeastern tile" or self.state ==
            "Hoe walking to northeastern tile" then
            self:hoeLand()
        elseif self.state == "Seed walking to southern tile" or self.state == "Seed walking to northern tile" or
            self.state == "Seed walking to eastern tile" or self.state == "Seed walking to southeastern tile" or
            self.state == "Seed walking to northeastern tile" then
            self:seedLand()
        elseif self.fx * 0.001 == self.waypointX and self.fy * 0.001 == self.waypointY and self.moveDir ~= "none" then
            if self.state == "Going to workplace" or self.state == "Going to hoe the land from south" or self.state ==
                "Going to hoe the land from north" or self.state == "Going to seed the land from south" or self.state ==
                "Going to seed the land from north" then
                if self:reachedPathEnd() then
                    if self.state == "Going to hoe the land from south" or self.state ==
                        "Going to hoe the land from north" then
                        self:hoeLand()
                    elseif self.state == "Going to seed the land from south" or self.state ==
                        "Going to seed the land from north" then
                        self:seedLand()
                    else
                        self.workplace:work(self)
                        self:clearPath()
                        self:updatePosition()
                    end
                    return
                else
                    self:setNextWaypoint()

                end
                self.count = self.count + 1
            elseif self.state == "Going to pick up hops" then
                if self:reachedPathEnd() then
                    self.resourceTile:takeResource()
                    self.hops = self.hops + 1
                    if self.hops < 8 then
                        self.workplace:work(self)
                        self:clearPath()
                    else
                        self.state = "Go to stockpile"
                        self:clearPath()
                        return
                    end
                else
                    self:setNextWaypoint()
                end
                self.count = self.count + 1
            elseif self.state == "Going to stockpile" then
                if self:reachedPathEnd() then
                    _G.stockpile:store("hop")
                    self.hops = 0
                    self.state = "Go to workplace"
                    self:clearPath()
                    return
                else
                    self:setNextWaypoint()

                end
                self.count = self.count + 1
            end
        end
    end
end

function HopsFarmer:animate()
    self:update()
    Worker.animate(self)
end

return HopsFarmer
